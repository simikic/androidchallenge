package org.simikic.androidchallenge.main;

import android.os.Bundle;

import org.simikic.androidchallenge.app.model.ImageMetadata;

import javax.inject.Inject;

public class MainPresenterImpl implements MainContract.Presenter {

    @Inject
    MainContract.View view;

    @Inject
    public MainPresenterImpl() {
    }

    @Override
    public void init(Bundle savedInstanceState) {
        view.setupNavigationManager(savedInstanceState);
    }

    @Override
    public void imageSelected(ImageMetadata imageMetadata) {
        view.showImagePreview(imageMetadata);
    }
}