package org.simikic.androidchallenge.main.gallery;

import android.Manifest;
import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.tbruyelle.rxpermissions2.RxPermissions;

import org.simikic.androidchallenge.app.helpers.IterableCursor;
import org.simikic.androidchallenge.app.model.ImageMetadata;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class GalleryPresenterImpl implements GalleryContract.Presenter {

    @Inject
    GalleryContract.View view;
    private CompositeDisposable compositeDisposable;
    private RxPermissions rxPermissions;

    @Inject
    public GalleryPresenterImpl() {
    }

    @Override
    public void init(Activity activity) {
        compositeDisposable = new CompositeDisposable();
        rxPermissions = new RxPermissions(activity);
        compositeDisposable.add(rxPermissions
                .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) {
                        loadData(activity);
                    } else {
                        view.showPermissionDeniedMsg();
                    }
                }));
    }

    private void loadData(Activity activity) {
        Cursor cursor = createCursor(activity);
        compositeDisposable.add(
                Observable
                        .fromIterable(IterableCursor.from(cursor))
                        .map(cur -> {
                            int idColumn = cur.getColumnIndex(MediaStore.Images.Media._ID);
                            int dataColumn = cur.getColumnIndex(MediaStore.Images.Media.DATA);
                            int nameColumn = cur.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
                            return new ImageMetadata(dataColumn, nameColumn, idColumn, cur);
                        })
                        .toList()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(list -> {
                            Timber.e("List size: %s", list.size());
                            view.setupList(list);
                        }));
    }

    private Cursor createCursor(Activity activity) {
        // which image properties are we querying
        String[] projection = new String[]{
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Images.Media.DATE_TAKEN
        };

        // content:// style URI for the "primary" external storage volume
        Uri images = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        // Make the query.
        return activity.managedQuery(images,
                projection, // Which columns to return
                null,       // Which rows to return (all rows)
                null,       // Selection arguments (none)
                null        // Ordering
        );
    }

    @Override
    public void deinit() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}