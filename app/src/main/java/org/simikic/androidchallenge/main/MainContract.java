package org.simikic.androidchallenge.main;

import android.os.Bundle;

import org.simikic.androidchallenge.app.model.ImageMetadata;

public interface MainContract {

    interface Presenter {
        void init(Bundle savedInstanceState);

        void imageSelected(ImageMetadata imageMetadata);
    }

    interface View {
        void setupNavigationManager(Bundle savedInstanceState);

        void showImagePreview(ImageMetadata imageMetadata);
    }
}
