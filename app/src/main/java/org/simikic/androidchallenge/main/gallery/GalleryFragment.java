package org.simikic.androidchallenge.main.gallery;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.simikic.androidchallenge.R;
import org.simikic.androidchallenge.app.helpers.BaseFragment;
import org.simikic.androidchallenge.app.helpers.GlideApp;
import org.simikic.androidchallenge.app.model.ImageMetadata;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import timber.log.Timber;

public class GalleryFragment extends BaseFragment implements GalleryContract.View, IGalleryListener {

    @Inject
    GalleryContract.Presenter presenter;

    @BindView(R.id.rv_image_grid)
    RecyclerView rvImageGrid;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_gallery;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(R.string.app_name);
        presenter.init(getActivity());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.deinit();
    }

    // region View
    @Override
    public void setupList(List<ImageMetadata> imageList) {
        Timber.e(Thread.currentThread().getName());
        rvImageGrid.setAdapter(new ImageAdapter(imageList, GlideApp.with(this), this));
    }

    @Override
    public void showPermissionDeniedMsg() {
        showToast(R.string.err_permission_denied);
    }

    // endregion

    @Override
    public void onImageClicked(ImageMetadata imageMetadata) {
        navigation.go2ImagePreview(imageMetadata);
    }
}
