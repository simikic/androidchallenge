package org.simikic.androidchallenge.main.gallery;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class GalleryModule {

    @Binds
    abstract GalleryContract.View provideGalleryView(GalleryFragment galleryFragment);

    @Binds
    abstract GalleryContract.Presenter provideGalleryPresenter(GalleryPresenterImpl galleryPresenter);
}
