package org.simikic.androidchallenge.main.preview;

import android.net.Uri;

import org.simikic.androidchallenge.app.model.ImageMetadata;

public interface ImagePreviewContract {

    interface Presenter {
        void init(ImageMetadata imageMetadata);

        void selectFilter();

        void selectHistogram();
    }

    interface View {
        void showImage(String imagePath);

        void showTitle(String description);

        void showFilterActivity(Uri imageUri);

        void showHistogramActivity(Uri imageUri);
    }
}