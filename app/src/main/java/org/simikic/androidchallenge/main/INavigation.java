package org.simikic.androidchallenge.main;

import org.simikic.androidchallenge.app.model.ImageMetadata;

public interface INavigation {

    void go2ImagePreview(ImageMetadata imageMetadata);
}
