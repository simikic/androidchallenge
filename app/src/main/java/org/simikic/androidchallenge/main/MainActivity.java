package org.simikic.androidchallenge.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.ncapdevi.fragnav.FragNavController;
import com.ncapdevi.fragnav.FragNavTransactionOptions;

import org.simikic.androidchallenge.R;
import org.simikic.androidchallenge.app.helpers.BaseActivity;
import org.simikic.androidchallenge.app.model.ImageMetadata;
import org.simikic.androidchallenge.main.gallery.GalleryFragment;
import org.simikic.androidchallenge.main.preview.ImagePreviewFragment;

import javax.inject.Inject;

public class MainActivity extends BaseActivity implements MainContract.View, INavigation {

    @Inject
    MainContract.Presenter presenter;

    private FragNavController navigationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter.init(savedInstanceState);
    }

    @Override
    public void onBackPressed() {
        if (navigationManager.getCurrentStack().size() > 1) {
            navigationManager.popFragment();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (navigationManager != null) {
            navigationManager.onSaveInstanceState(outState);
        }
    }

    // region View
    @Override
    public void setupNavigationManager(Bundle savedInstanceState) {
        navigationManager = FragNavController
                .newBuilder(savedInstanceState, getSupportFragmentManager(), R.id.fragment_container)
                .defaultTransactionOptions(FragNavTransactionOptions.newBuilder().allowStateLoss(true).build())
                .transactionListener(new FragNavController.TransactionListener() {
                    @Override
                    public void onTabTransaction(@Nullable Fragment fragment, int i) {
                    }

                    @Override
                    public void onFragmentTransaction(Fragment fragment, FragNavController.TransactionType transactionType) {
                        if (navigationManager != null) {
                            getSupportActionBar().setDisplayHomeAsUpEnabled(!navigationManager.isRootFragment());
                        }
                    }
                })
                .rootFragment(new GalleryFragment())
                .build();
    }

    @Override
    public void showImagePreview(ImageMetadata imageMetadata) {
        navigationManager.pushFragment(ImagePreviewFragment.newInstance(imageMetadata));
    }

    @Override
    public void go2ImagePreview(ImageMetadata imageMetadata) {
        presenter.imageSelected(imageMetadata);
    }
    // endregion

}
