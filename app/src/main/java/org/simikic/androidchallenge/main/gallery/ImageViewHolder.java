package org.simikic.androidchallenge.main.gallery;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.simikic.androidchallenge.R;
import org.simikic.androidchallenge.app.helpers.GlideRequests;
import org.simikic.androidchallenge.app.model.ImageMetadata;

import butterknife.BindView;
import butterknife.ButterKnife;

class ImageViewHolder extends RecyclerView.ViewHolder {
    private final GlideRequests glideRequests;

    @BindView(R.id.iv_image)
    ImageView ivIcon;
    @BindView(R.id.tv_description)
    TextView tvCategoryName;

    public ImageViewHolder(View view, GlideRequests glideRequests, ImageAdapter.IImageListener callback) {
        super(view);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(v -> callback.onItemClicked(getAdapterPosition()));
        this.glideRequests = glideRequests;
    }

    public void bind(ImageMetadata imageMetadata) {
        glideRequests.load(imageMetadata.getImagePath()).placeholder(R.drawable.ic_placeholder).into(ivIcon);
        tvCategoryName.setText(imageMetadata.getDescription());
    }
}