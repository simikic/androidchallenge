package org.simikic.androidchallenge.main.preview;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import org.simikic.androidchallenge.R;
import org.simikic.androidchallenge.app.helpers.AppConstants;
import org.simikic.androidchallenge.app.helpers.BaseFragment;
import org.simikic.androidchallenge.app.helpers.GlideApp;
import org.simikic.androidchallenge.app.model.ImageMetadata;
import org.simikic.androidchallenge.filter.FilterActivity;
import org.simikic.androidchallenge.histogram.HistogramActivity;

import javax.inject.Inject;

import butterknife.BindView;

import static org.simikic.androidchallenge.app.helpers.AppConstants.ARG_IMAGE_METADATA;

public class ImagePreviewFragment extends BaseFragment implements ImagePreviewContract.View {

    @Inject
    ImagePreviewContract.Presenter presenter;

    @BindView(R.id.iv_image)
    ImageView ivImage;

    public static ImagePreviewFragment newInstance(ImageMetadata imageMetadata) {
        Bundle args = new Bundle();

        ImagePreviewFragment fragment = new ImagePreviewFragment();
        args.putParcelable(ARG_IMAGE_METADATA, imageMetadata);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_image_preview;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.init(getArguments().getParcelable(AppConstants.ARG_IMAGE_METADATA));
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.tools, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_filter:
                presenter.selectFilter();
                return true;
            case R.id.menu_histogram:
                presenter.selectHistogram();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // region View
    @Override
    public void showImage(String imagePath) {
        GlideApp.with(this).load(imagePath).placeholder(R.drawable.ic_placeholder).into(ivImage);
    }

    @Override
    public void showTitle(String description) {
        getActivity().setTitle(description);
    }

    @Override
    public void showFilterActivity(Uri imageUri) {
        Intent myIntent = new Intent(getActivity(), FilterActivity.class);
        myIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        startActivity(myIntent);
    }

    @Override
    public void showHistogramActivity(Uri imageUri) {
        Intent myIntent = new Intent(getActivity(), HistogramActivity.class);
        myIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        startActivity(myIntent);
    }

    //  endregion
}
