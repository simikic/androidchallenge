package org.simikic.androidchallenge.main.preview;


import org.simikic.androidchallenge.app.model.ImageMetadata;

import javax.inject.Inject;

public class ImagePreviewPresenterImpl implements ImagePreviewContract.Presenter {

    @Inject
    ImagePreviewContract.View view;

    private ImageMetadata imageMetadata;

    @Inject
    public ImagePreviewPresenterImpl() {
    }

    @Override
    public void init(ImageMetadata imageMetadata) {
        this.imageMetadata = imageMetadata;
        view.showTitle(imageMetadata.getDescription());
        view.showImage(imageMetadata.getImagePath());
    }

    @Override
    public void selectFilter() {
        view.showFilterActivity(imageMetadata.getUri());
    }

    @Override
    public void selectHistogram() {
        view.showHistogramActivity(imageMetadata.getUri());
    }
}
