package org.simikic.androidchallenge.main.preview;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ImagePreviewModule {

    @Binds
    abstract ImagePreviewContract.View provideImagePreviewView(ImagePreviewFragment imagePreviewFragment);

    @Binds
    abstract ImagePreviewContract.Presenter provideImagePreviewPresenter(ImagePreviewPresenterImpl imagePreviewPresenter);
}
