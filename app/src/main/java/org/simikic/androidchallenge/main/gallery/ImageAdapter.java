package org.simikic.androidchallenge.main.gallery;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.simikic.androidchallenge.R;
import org.simikic.androidchallenge.app.helpers.GlideRequests;
import org.simikic.androidchallenge.app.model.ImageMetadata;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageViewHolder> {

    private final GlideRequests glideRequests;
    private final List<ImageMetadata> items;
    private final IGalleryListener callback;

    public ImageAdapter(List<ImageMetadata> imageList, GlideRequests glideRequests, IGalleryListener callback) {
        this.items = imageList;
        this.glideRequests = glideRequests;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.li_image, viewGroup, false);
        return new ImageViewHolder(view, glideRequests, index -> callback.onImageClicked(items.get(index)));
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder viewHolder, int i) {
        viewHolder.bind(items.get(i));
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    interface IImageListener {
        void onItemClicked(int index);
    }

}
