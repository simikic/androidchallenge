package org.simikic.androidchallenge.main.gallery;

import android.app.Activity;

import org.simikic.androidchallenge.app.model.ImageMetadata;

import java.util.List;

public interface GalleryContract {

    interface Presenter {
        void deinit();

        void init(Activity activity);
    }

    interface View {
        void setupList(List<ImageMetadata> imageList);

        void showPermissionDeniedMsg();
    }
}
