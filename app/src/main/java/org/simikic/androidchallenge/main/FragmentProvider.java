package org.simikic.androidchallenge.main;

import org.simikic.androidchallenge.app.FragmentScope;
import org.simikic.androidchallenge.main.gallery.GalleryFragment;
import org.simikic.androidchallenge.main.gallery.GalleryModule;
import org.simikic.androidchallenge.main.preview.ImagePreviewFragment;
import org.simikic.androidchallenge.main.preview.ImagePreviewModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentProvider {
    @ContributesAndroidInjector(modules = GalleryModule.class)
    @FragmentScope
    abstract GalleryFragment provideGalleryFragmentFactory();


    @ContributesAndroidInjector(modules = ImagePreviewModule.class)
    @FragmentScope
    abstract ImagePreviewFragment provideImagePreviewFragmentFactory();
}