package org.simikic.androidchallenge.main.gallery;

import org.simikic.androidchallenge.app.model.ImageMetadata;

public interface IGalleryListener {

    void onImageClicked(ImageMetadata imageMetadata);
}
