package org.simikic.androidchallenge.app.helpers;

import android.database.Cursor;

import java.util.Iterator;

public class IterableCursor implements Iterable<Cursor> {

    private Cursor mIterableCursor;

    public IterableCursor(Cursor c) {
        mIterableCursor = c;
    }

    public static IterableCursor from(Cursor c) {
        return new IterableCursor(c);
    }

    @Override
    public Iterator<Cursor> iterator() {
        return RxCursorIterator.from(mIterableCursor);
    }

    static class RxCursorIterator implements Iterator<Cursor> {

        private final Cursor mCursor;

        public RxCursorIterator(Cursor cursor) {
            mCursor = cursor;
        }

        public static Iterator<Cursor> from(Cursor cursor) {
            return new RxCursorIterator(cursor);
        }

        @Override
        public boolean hasNext() {
            return !mCursor.isClosed() && mCursor.moveToNext();
        }

        @Override
        public Cursor next() {
            return mCursor;
        }
    }
}