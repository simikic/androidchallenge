package org.simikic.androidchallenge.app.model;

import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.MediaStore;

public class ImageMetadata implements Parcelable {

    private final String imagePath;
    private final String description;
    private final Uri uri;

    public ImageMetadata(int dataColumn, int nameColumn, int idColumn, Cursor cur) {
        // Get the field values
        imagePath = cur.getString(dataColumn);
        description = cur.getString(nameColumn);
        uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, Integer.toString(cur.getInt(idColumn)));
    }

    public String getImagePath() {
        return imagePath;
    }

    public String getDescription() {
        return description;
    }

    public Uri getUri() {
        return uri;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imagePath);
        dest.writeString(this.description);
        dest.writeParcelable(this.uri, flags);
    }

    protected ImageMetadata(Parcel in) {
        this.imagePath = in.readString();
        this.description = in.readString();
        this.uri = in.readParcelable(Uri.class.getClassLoader());
    }

    public static final Parcelable.Creator<ImageMetadata> CREATOR = new Parcelable.Creator<ImageMetadata>() {
        @Override
        public ImageMetadata createFromParcel(Parcel source) {
            return new ImageMetadata(source);
        }

        @Override
        public ImageMetadata[] newArray(int size) {
            return new ImageMetadata[size];
        }
    };
}
