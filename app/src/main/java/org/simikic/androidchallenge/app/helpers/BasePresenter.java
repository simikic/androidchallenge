package org.simikic.androidchallenge.app.helpers;

public interface BasePresenter {
    void init();

    void deinit();
}
