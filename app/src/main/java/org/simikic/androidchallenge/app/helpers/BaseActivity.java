package org.simikic.androidchallenge.app.helpers;

import android.view.MenuItem;
import android.widget.Toast;

import dagger.android.support.DaggerAppCompatActivity;

public class BaseActivity extends DaggerAppCompatActivity {

    private Toast mToast;

    public void showToast(int textRes) {
        showToast(getString(textRes));
    }

    public void showToast(CharSequence text) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
        mToast.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                this.onBackPressed();
                return true;
            default:
                return false;
        }
    }
}
