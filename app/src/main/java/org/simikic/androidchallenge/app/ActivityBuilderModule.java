package org.simikic.androidchallenge.app;


import org.simikic.androidchallenge.filter.FilterActivity;
import org.simikic.androidchallenge.filter.FilterModule;
import org.simikic.androidchallenge.histogram.HistogramActivity;
import org.simikic.androidchallenge.histogram.HistogramModule;
import org.simikic.androidchallenge.main.FragmentProvider;
import org.simikic.androidchallenge.main.MainActivity;
import org.simikic.androidchallenge.main.MainModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilderModule {

    @ContributesAndroidInjector(modules = {MainModule.class, FragmentProvider.class})
    @ActivityScope
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = {FilterModule.class})
    @ActivityScope
    abstract FilterActivity bindFilterActivity();

    @ContributesAndroidInjector(modules = {HistogramModule.class})
    @ActivityScope
    abstract HistogramActivity bindHistogramActivity();
}
