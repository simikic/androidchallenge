package org.simikic.androidchallenge.filter;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntKey;
import dagger.multibindings.IntoMap;
import jp.co.cyberagent.android.gpuimage.GPUImageColorInvertFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageGrayscaleFilter;

@Module
public abstract class FilterModule {

    @Binds
    abstract FilterContract.View provideFilterView(FilterActivity filterActivity);

    @Binds
    abstract FilterContract.Presenter provideFilterPresenter(FilterPresenterImpl filterPresenter);

    @Provides
    @IntoMap
    @IntKey(FilterHelpers.FILTER_NEGATIVE)
    static GPUImageFilter provideNegativeFilter() {
        return new GPUImageColorInvertFilter();
    }

    @Provides
    @IntoMap
    @IntKey(FilterHelpers.FILTER_GRAYSCALE)
    static GPUImageFilter provideGrayscaleFilter() {
        return new GPUImageGrayscaleFilter();
    }
}