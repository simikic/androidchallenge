package org.simikic.androidchallenge.filter;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.simikic.androidchallenge.R;
import org.simikic.androidchallenge.app.helpers.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;

public class FilterActivity extends BaseActivity implements FilterContract.View {

    @BindView(R.id.gpuiv_image)
    GPUImageView gpuImage;
    @BindView(R.id.tv_missing_error)
    TextView tvMissingData;

    @Inject
    FilterContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Uri imageUri = getIntent() != null ? getIntent().getParcelableExtra(Intent.EXTRA_STREAM) : null;
        if (imageUri != null) {
            presenter.init(imageUri);
        } else {
            presenter.dataIsMissing();
        }
    }

    @OnClick({R.id.btn_grayscale, R.id.btn_negative})
    public void onFilterSelected(View view) {
        switch (view.getId()) {
            case R.id.btn_grayscale:
                presenter.selectFilter(FilterHelpers.FILTER_GRAYSCALE);
                break;
            case R.id.btn_negative:
                presenter.selectFilter(FilterHelpers.FILTER_NEGATIVE);
                break;
        }
    }

    @Override
    public void loadImage(Uri imageUri) {
        gpuImage.setScaleType(GPUImage.ScaleType.CENTER_INSIDE);
        gpuImage.setImage(imageUri);
    }

    @Override
    public void applyFilter(GPUImageFilter filter) {
        gpuImage.setFilter(filter);
    }

    @Override
    public void showMissingData() {
        showToast(R.string.err_missing_data);
        tvMissingData.setVisibility(View.VISIBLE);
    }
}
