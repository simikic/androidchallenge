package org.simikic.androidchallenge.filter;

import android.net.Uri;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

public interface FilterContract {

    interface Presenter {
        void init(Uri imageUri);

        void selectFilter(int filterIndex);

        void dataIsMissing();
    }

    interface View {
        void loadImage(Uri imageUri);

        void applyFilter(GPUImageFilter filter);

        void showMissingData();
    }
}
