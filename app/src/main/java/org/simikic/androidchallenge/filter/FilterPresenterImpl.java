package org.simikic.androidchallenge.filter;

import android.net.Uri;

import java.util.Map;

import javax.inject.Inject;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

public class FilterPresenterImpl implements FilterContract.Presenter {

    @Inject
    FilterContract.View view;
    @Inject
    Map<Integer, GPUImageFilter> filterMap;

    @Inject
    public FilterPresenterImpl() {
    }

    @Override
    public void init(Uri imageUri) {
        view.loadImage(imageUri);
    }

    @Override
    public void selectFilter(int filterIndex) {
        view.applyFilter(filterMap.get(filterIndex));
    }

    @Override
    public void dataIsMissing() {
        view.showMissingData();
    }
}