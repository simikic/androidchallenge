package org.simikic.androidchallenge.histogram;

import android.net.Uri;

public interface HistogramContract {

    interface Presenter {
        void init(Uri imageUri);
    }

    interface View {
    }
}
