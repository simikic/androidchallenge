package org.simikic.androidchallenge.histogram;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import org.simikic.androidchallenge.R;
import org.simikic.androidchallenge.app.helpers.BaseActivity;

import javax.inject.Inject;

import butterknife.ButterKnife;

public class HistogramActivity extends BaseActivity implements HistogramContract.View {


    @Inject
    HistogramContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histogram);
        ButterKnife.bind(this);

        Uri imageUri = getIntent() != null ? getIntent().getParcelableExtra(Intent.EXTRA_STREAM) : null;
        if (imageUri != null) {
//            presenter.init(imageUri);
        } else {
        }
    }
}
