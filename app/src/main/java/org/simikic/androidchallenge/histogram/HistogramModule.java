package org.simikic.androidchallenge.histogram;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class HistogramModule {

    @Binds
    abstract HistogramContract.View provideHistogramView(HistogramActivity histogramActivity);

    @Binds
    abstract HistogramContract.Presenter provideHistogramPresenter(HistogramPresenterImpl histogramPresenter);

}