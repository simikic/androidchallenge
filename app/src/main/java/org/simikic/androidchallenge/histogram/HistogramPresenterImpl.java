package org.simikic.androidchallenge.histogram;

import android.net.Uri;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class HistogramPresenterImpl implements HistogramContract.Presenter {

    @Inject
    HistogramContract.View view;
    private CompositeDisposable compositeDisposable;

    @Inject
    public HistogramPresenterImpl() {
    }

    @Override
    public void init(Uri imageUri) {
        compositeDisposable = new CompositeDisposable();
//        compositeDisposable.add(
//                Observable
//                        .just(imageUri)
//                        .map( imageUri -> {
//                            GlideApp.with()
//                        })
// solution was based on this comment :
//        https://stackoverflow.com/a/18103231/2960949
    }
}